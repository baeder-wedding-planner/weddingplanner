import { connect } from '../src/connection'
import { ExpenseDAO } from '../src/daos/expense-dao';
import { ExpenseDAOImpl } from '../src/daos/expense-dao-impl';
import { WeddingDAO } from '../src/daos/wedding-dao';
import { WeddingDAOImpl } from '../src/daos/wedding-dao-impl';
import { Expense, Wedding } from '../src/entities';

test("Should Create A Connection", async ()=>{
    const result = await connect.query('select * from wedding');
})

const weddingDAO:WeddingDAO = new WeddingDAOImpl();
const expenseDAO:ExpenseDAO = new ExpenseDAOImpl();

const testWedding:Wedding = new Wedding(0,"2021-12-12","Fresno","Rise",400);

test("Create a wedding, changed ID", async ()=>{
    const result:Wedding = await weddingDAO.createWedding(testWedding);
    expect(result.primaryKey).not.toBe(0);
})
test("Get wedding by ID", async ()=>{
    let wedding:Wedding = new Wedding(0,"2022-3-22","Reno","Jones",500);
    wedding = await weddingDAO.createWedding(wedding);
    let retrievedWedding:Wedding = await weddingDAO.getWeddingByPrimaryKey(wedding.primaryKey);
    expect(retrievedWedding.primaryKey).toBe(wedding.primaryKey);
});
test("Get all weddings", async ()=>{
    const wedding1:Wedding = new Wedding(0,"2021-12-30","Transylvania","Dracula",100);
    const wedding2:Wedding = new Wedding(0,"2021-12-25","Hawaii","Claus",0);
    const wedding3:Wedding = new Wedding(0,"2040-08-17","Vegas","Chance",5000);
    await weddingDAO.createWedding(wedding1);
    await weddingDAO.createWedding(wedding2);
    await weddingDAO.createWedding(wedding3);
    const weddings:Wedding[]= await weddingDAO.getAllWeddings();
    expect(weddings.length).toBeGreaterThanOrEqual(3);
});

test("Update Wedding, wedding name changed", async ()=>{
    let wedding:Wedding = new Wedding(0,"2022-2-22",'Castle','Jackson',6);
    wedding = await weddingDAO.createWedding(wedding);
    wedding.lname = "Updated"
    wedding = await weddingDAO.updateWedding(wedding);
    expect(wedding.lname).toBe("Updated");
});

test("Update Wedding, wedding date changed", async ()=>{
    let wedding:Wedding = new Wedding(0,"2022-2-28",'Columbus','Jackson',6);
    wedding = await weddingDAO.createWedding(wedding);
    wedding.weddingDate = "2022-2-25"
    wedding = await weddingDAO.updateWedding(wedding);
    expect(wedding.weddingDate).toBe("2022-2-25");
});

test("Update Wedding, wedding location changed", async ()=>{
    let wedding:Wedding = new Wedding(0,"2022-2-28",'','Johnson',6);
    wedding = await weddingDAO.createWedding(wedding);
    wedding.locale = "Updated"
    wedding = await weddingDAO.updateWedding(wedding);
    expect(wedding.locale).toBe("Updated");
});

test("Update Wedding, wedding budget changed", async ()=>{
    let wedding:Wedding = new Wedding(0,"2022-2-28",'Detroit','Brit',6);
    wedding = await weddingDAO.createWedding(wedding);
    wedding.budget = 50000
    wedding = await weddingDAO.updateWedding(wedding);
    expect(wedding.budget).toBe(50000);
});

test("Delete Wedding", async ()=>{
    let wedding:Wedding = new Wedding(0,'2022-1-1','Delete','Me',0);
    wedding = await weddingDAO.createWedding(wedding);
    const result:boolean = await weddingDAO.deleteWeddingByPrimaryKey(wedding.primaryKey);
    expect(result).toBeTruthy();
});

test("Create an expense, changed primary key", async ()=>{
    const temp:Wedding = new Wedding(0,"2022-4-5","Los Angeles","Jones",0);
    const tw = await weddingDAO.createWedding(temp);
    const testAcc = new Expense(0,"",1000,"",tw.primaryKey);
    const result:Expense = await expenseDAO.createExpense(testAcc, tw.primaryKey);
    expect(result.primaryKey).not.toBe(0);
})

test("Create an expense, foreign key same as attached wedding", async ()=>{
    const temp:Wedding = new Wedding(0,"2022-5-22","Denver","Schmoe",0);
    const tw = await weddingDAO.createWedding(temp);
    const testExp = new Expense(0,"",1000,"",tw.primaryKey);
    const result:Expense = await expenseDAO.createExpense(testExp, tw.primaryKey);
    expect(result.foreignKey).toBe(tw.primaryKey);
})

test("Get expense by primary key", async ()=>{
    let expense:Expense = new Expense(0,"",1500,"",1);
    expense = await expenseDAO.createExpense(expense, expense.foreignKey);
    let retrievedExpense:Expense = await expenseDAO.getExpenseByPrimaryKey(expense.primaryKey);
    expect(retrievedExpense.primaryKey).toBe(expense.primaryKey);
});

test("Get all expenses by foreign key", async ()=>{
    let expense:Expense = new Expense(0,"",2100,"",1);
    expense = await expenseDAO.createExpense(expense, expense.foreignKey);
    let retrievedExpenses:Expense[] = await expenseDAO.getAllExpensesByForeignKey(expense.foreignKey);
    expect(retrievedExpenses[0].foreignKey).toBe(expense.foreignKey);
});

test("Get all expenses", async ()=>{
    const temp:Wedding = new Wedding(0,"2022-8-8","Atlanta","Jurley",0);
    const tw = await weddingDAO.createWedding(temp);
    let exp1:Expense = new Expense(0,"",0,"",tw.primaryKey);
    let exp2:Expense = new Expense(0,"",0,"",tw.primaryKey);
    await expenseDAO.createExpense(exp1, exp1.foreignKey);
    await expenseDAO.createExpense(exp2, exp2.foreignKey);
    const expenses:Expense[]= await expenseDAO.getAllExpenses();
    expect(expenses.length).toBeGreaterThanOrEqual(2);
});

test("Update Expense, amount correct", async ()=>{
    let expense:Expense = new Expense(0,"",0,"",1);
    expense = await expenseDAO.createExpense(expense,expense.foreignKey);
    expense.amount = 250
    expense = await expenseDAO.updateExpense(expense);
    expect(expense.amount).toBe(250);
});

test("Update Expense, reason correct", async ()=>{
    let expense:Expense = new Expense(0,"",0,"",1);
    expense = await expenseDAO.createExpense(expense,expense.foreignKey);
    expense.reason = "Changed Reason";
    expense = await expenseDAO.updateExpense(expense);
    expect(expense.reason).toBe("Changed Reason");
});

test("Update Expense, attachment correct", async ()=>{
    let expense:Expense = new Expense(0,"",0,"incorrect",1);
    expense = await expenseDAO.createExpense(expense,expense.foreignKey);
    expense.attachment = ""
    expense = await expenseDAO.updateExpense(expense);
    expect(expense.attachment).toBe("");
});

test("Delete Expense by primary key", async ()=>{
    let expense:Expense = new Expense(0,"",0,"",1);
    expense = await expenseDAO.createExpense(expense, expense.foreignKey);
    const result:boolean = await expenseDAO.deleteExpenseByPrimaryKey(expense.primaryKey);
    expect(result).toBeTruthy();
});

test("Delete all Expenses by foreign key", async ()=>{
    const temp:Wedding = new Wedding(0,"2022-3-3","Delete","Expenses",0);
    const tw = await weddingDAO.createWedding(temp);
    const exp1 = new Expense(0,"",500,"",tw.primaryKey);
    const exp2 = new Expense(0,"",500,"",tw.primaryKey);
    const exp3 = new Expense(0,"",500,"",tw.primaryKey);
    await expenseDAO.createExpense(exp1, exp1.foreignKey);
    await expenseDAO.createExpense(exp2, exp2.foreignKey);
    await expenseDAO.createExpense(exp3, exp3.foreignKey);
    const result:boolean = await expenseDAO.deleteAllExpensesByForeignKey(tw.primaryKey);
    expect(result).toBeTruthy();
});

afterAll(()=>{
    connect.end();
})