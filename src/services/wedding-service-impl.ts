import { ExpenseDAO } from "../daos/expense-dao";
import { ExpenseDAOImpl } from "../daos/expense-dao-impl";
import { WeddingDAO } from "../daos/wedding-dao";
import { WeddingDAOImpl } from "../daos/wedding-dao-impl";
import { Wedding } from "../entities";
import { MissingResourceError } from "../errors";
import WeddingService from "./wedding-service";

export class WeddingServiceImpl implements WeddingService{

    weddingDAO:WeddingDAO = new WeddingDAOImpl();
    expenseDAO:ExpenseDAO = new ExpenseDAOImpl();

    registerWedding(wedding: Wedding): Promise<Wedding> {
        return this.weddingDAO.createWedding(wedding);
    }
    retrieveAllWeddings(): Promise<Wedding[]> {
        return this.weddingDAO.getAllWeddings();
    }
    retrieveWeddingByPrimaryKey(primaryKey: number): Promise<Wedding> {
            return this.weddingDAO.getWeddingByPrimaryKey(primaryKey);
    }
    async SearchByName(lastName: string): Promise<Wedding[]> {
        let weddings:Wedding[] = await this.weddingDAO.getAllWeddings();
        let gotten = [];
        for(let i = 0; i < weddings.length; i++){
            if(weddings[i].lname === lastName){
                gotten.push(weddings[i]);
            }
        }
        if(weddings.length === 0){
            throw new MissingResourceError(`Could not find any weddings for the name ${lastName}`)
        }
        return gotten;
    }
    async modifyWedding(wedding: Wedding): Promise<Wedding> {
        try{
            const check = await this.weddingDAO.getWeddingByPrimaryKey(wedding.primaryKey);
            return this.weddingDAO.updateWedding(wedding);
        } catch(error) {
            throw new MissingResourceError(`Could not find wedding with ID ${wedding.primaryKey}`);
        }
    }
    async removeWeddingByPrimaryKey(primaryKey: number): Promise<boolean> {
        try{
            const check = await this.weddingDAO.getWeddingByPrimaryKey(primaryKey);
            await this.expenseDAO.deleteAllExpensesByForeignKey(primaryKey);
            return this.weddingDAO.deleteWeddingByPrimaryKey(primaryKey);
        } catch(error){
            throw new MissingResourceError(`Could not find wedding with ID ${primaryKey}`);
        }
    }
    
}