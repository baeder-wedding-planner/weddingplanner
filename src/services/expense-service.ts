import { Expense } from "../entities";

export default interface ExpenseService{
    registerExpense(expense:Expense, foreignKey:number):Promise<Expense>;

    retrieveAllExpenses():Promise<Expense[]>;

    retrieveExpenseByPrimaryKey(primaryKey:number):Promise<Expense>;

    retrieveAllExpensesByForeignKey(foreignKey:number):Promise<Expense[]>;

    modifyExpense(expense:Expense):Promise<Expense>;

    removeExpenseByPrimaryKey(primaryKey:number):Promise<boolean>;

    removeAllExpensesByForeignKey(foreignKey:number):Promise<boolean>;
}