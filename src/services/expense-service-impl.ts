import { ExpenseDAO } from "../daos/expense-dao";
import { ExpenseDAOImpl } from "../daos/expense-dao-impl";
import { WeddingDAO } from "../daos/wedding-dao";
import { WeddingDAOImpl } from "../daos/wedding-dao-impl";
import { Expense } from "../entities";
import { MissingResourceError } from "../errors";
import ExpenseService from "./expense-service";

export class ExpenseServiceImpl implements ExpenseService{

    expenseDAO:ExpenseDAO = new ExpenseDAOImpl();
    weddingDAO:WeddingDAO = new WeddingDAOImpl();

    async registerExpense(expense: Expense, foreignKey: number): Promise<Expense> {
        try {
            const check = await this.weddingDAO.getWeddingByPrimaryKey(foreignKey);
            return this.expenseDAO.createExpense(expense, foreignKey);
        } catch(error) {
            throw new MissingResourceError(`Could not find wedding with ID ${foreignKey}`);
        }
    }
    retrieveAllExpenses(): Promise<Expense[]> {
        return this.expenseDAO.getAllExpenses();
    }
    retrieveExpenseByPrimaryKey(primaryKey: number): Promise<Expense> {
            return this.expenseDAO.getExpenseByPrimaryKey(primaryKey);
    }
    async retrieveAllExpensesByForeignKey(foreignKey: number): Promise<Expense[]> {
        try {
            const check = await this.weddingDAO.getWeddingByPrimaryKey(foreignKey);
            return this.expenseDAO.getAllExpensesByForeignKey(foreignKey);
        } catch(error) {
            throw new MissingResourceError(`Could not find wedding with ID ${foreignKey}`);
        }
    }
    async modifyExpense(expense: Expense): Promise<Expense> {
        try{
            const check = await this.expenseDAO.getExpenseByPrimaryKey(expense.primaryKey);
            return this.expenseDAO.updateExpense(expense);
        } catch(error) {
            throw new MissingResourceError(`Could not find expense with ID ${expense.primaryKey}`);
        }
    }
    async removeExpenseByPrimaryKey(primaryKey: number): Promise<boolean> {
        try{
            const check = await this.expenseDAO.getExpenseByPrimaryKey(primaryKey);
            return this.expenseDAO.deleteExpenseByPrimaryKey(primaryKey);
        } catch(error) {
            throw new MissingResourceError(`Could not find expense with ID ${primaryKey}`);
        }
    }
    async removeAllExpensesByForeignKey(foreignKey: number): Promise<boolean> {
        try {
            const check = await this.weddingDAO.getWeddingByPrimaryKey(foreignKey);
            return this.expenseDAO.deleteAllExpensesByForeignKey(foreignKey);
        } catch(error) {
            throw new MissingResourceError(`Could not find wedding with ID ${foreignKey}`);
        }
    }

}