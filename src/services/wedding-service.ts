import { Wedding } from "../entities";

export default interface WeddingService{
    registerWedding(wedding:Wedding):Promise<Wedding>;

    retrieveAllWeddings():Promise<Wedding[]>;

    retrieveWeddingByPrimaryKey(primaryKey:number):Promise<Wedding>;

    SearchByName(firstName:string):Promise<Wedding[]>;

    modifyWedding(wedding:Wedding):Promise<Wedding>;

    removeWeddingByPrimaryKey(primaryKey:number):Promise<boolean>;
}