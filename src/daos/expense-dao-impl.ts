import { Expense } from "../entities";
import { ExpenseDAO } from "./expense-dao";
import { connect } from "../connection"
import { MissingResourceError } from "../errors";

export class ExpenseDAOImpl implements ExpenseDAO{
    async createExpense(expense: Expense, foreignKey: number): Promise<Expense> {
        const sql:string = 'insert into expense(reason,amount,attachment,foreign_key) values ($1,$2,$3,$4) returning primary_key';
        const values = [
            expense.reason,
            Number(expense.amount),
            expense.attachment,
            foreignKey
        ];
        const result = await connect.query(sql, values);
        expense.primaryKey = result.rows[0].primary_key;
        expense.foreignKey = foreignKey;
        return expense;
    }
    async getAllExpenses(): Promise<Expense[]> {
        const sql:string = "select * from expense";
        const result = await connect.query(sql);
        const expenses:Expense[] = [];
        for(const row of result.rows){
            const expense:Expense = new Expense(
                row.primary_key,
                row.reason,
                Number(row.amount),
                row.attachment,
                row.foreign_key);
            expenses.push(expense);
        }
        return expenses;
    }
    async getExpenseByPrimaryKey(primaryKey: number): Promise<Expense> {
        const sql:string = 'select * from expense where primary_key = $1';
        const values = [primaryKey];
        const result = await connect.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`Could not find expense with ID ${primaryKey}`);
        }
        const row = result.rows[0];
        const expense:Expense = new Expense(
            row.primary_key,
            row.reason,
            Number(row.amount),
            row.attachment,
            row.foreign_key);
        return expense;
    }
    async getAllExpensesByForeignKey(foreignKey: number): Promise<Expense[]> {
        const sql:string = 'select * from expense where foreign_key = $1';
        const values = [foreignKey];
        const result = await connect.query(sql, values);
        const expenses:Expense[] = [];
        for(let i = 0; i < result.rowCount;i++){
            const row = result.rows[i];
            const expense:Expense = new Expense(
                row.primary_key,
                row.reason,
                Number(row.amount),
                row.attachment,
                row.foreign_key);
            expenses.push(expense);
        }
        return expenses;
    }
    async updateExpense(expense: Expense): Promise<Expense> {
        const sql:string = 'update expense set reason=$1, amount=$2, attachment=$3 where primary_key=$4'
        const values = [
            expense.reason,
            Number(expense.amount),
            expense.attachment,
            expense.primaryKey];
        const result = await connect.query(sql,values);
        return expense;
    }
    async deleteExpenseByPrimaryKey(primaryKey: number): Promise<boolean> {
        const sql:string = 'delete from expense where primary_key = $1'
        const values = [primaryKey]
        const result = await connect.query(sql,values);
        if(result.rowCount > 0) {
            return true;
        } else {
            return false;
        }
    }
    async deleteAllExpensesByForeignKey(foreignKey: number): Promise<boolean> {
        const sql:string = 'delete from expense where foreign_key = $1'
        const values = [foreignKey]
        const result = await connect.query(sql,values);
        if(result.rowCount > 0){
            return true;
        } else {
            return false;
        }
    }
    
}