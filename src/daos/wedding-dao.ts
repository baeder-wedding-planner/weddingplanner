import { Wedding } from "../entities";
export interface WeddingDAO{
    // CREATE
    createWedding(wedding:Wedding):Promise<Wedding>;
    // READ
    getAllWeddings():Promise<Wedding[]>;
    getWeddingByPrimaryKey(primaryKey:number):Promise<Wedding>;
    // UPDATE
    updateWedding(wedding:Wedding):Promise<Wedding>;
    // DELETE
    deleteWeddingByPrimaryKey(primaryKey:number):Promise<boolean>;
}