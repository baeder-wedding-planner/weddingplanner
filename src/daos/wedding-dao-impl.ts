import { Wedding } from "../entities";
import { WeddingDAO } from "./wedding-dao";
import { connect } from "../connection";
import { MissingResourceError } from "../errors";

export class WeddingDAOImpl implements WeddingDAO{
    async createWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = 'insert into wedding(wedding_date,locale,lname,budget) values ($1,$2,$3,$4) returning primary_key';
        const values = [
            wedding.weddingDate,
            wedding.locale,
            wedding.lname,
            wedding.budget
        ];
        const result = await connect.query(sql, values);
        wedding.primaryKey = result.rows[0].primary_key;
        return wedding;
    }
    async getAllWeddings(): Promise<Wedding[]> {
        const sql:string = "select * from wedding";
        const result = await connect.query(sql);
        const weddings:Wedding[] = [];
        for(const row of result.rows){
            const wedding:Wedding = new Wedding(
                row.primary_key,
                row.wedding_date,
                row.locale,
                row.lname,
                row.budget);
            weddings.push(wedding);
        }
        return weddings;
    }
    async getWeddingByPrimaryKey(primaryKey: number): Promise<Wedding> {
        const sql:string = 'select * from wedding where primary_key = $1';
        const values = [primaryKey];
        const result = await connect.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`Could not find wedding with ID ${primaryKey}`)
        }
        const row = result.rows[0];
        const wedding:Wedding = new Wedding(
            row.primary_key,
            row.wedding_date,
            row.locale,
            row.lname,
            row.budget);
        return wedding;
    }
    async updateWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = 'update wedding set wedding_date=$1, locale=$2, lname=$3, budget=$4 where primary_key=$5'
        const values = [
            wedding.weddingDate,
            wedding.locale,
            wedding.lname,
            wedding.budget,
            wedding.primaryKey];
        const result = await connect.query(sql,values);
        return wedding;
    }
    async deleteWeddingByPrimaryKey(primaryKey: number): Promise<boolean> {
        const sql:string = 'delete from wedding where primary_key = $1'
        const values = [primaryKey]
        const result = await connect.query(sql,values);
        if (result)
        return true;
    }
    
}