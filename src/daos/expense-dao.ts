import { Expense, Wedding } from "../entities";
export interface ExpenseDAO{
    // CREATE
    createExpense(expense:Expense,foreignKey:number):Promise<Expense>;
    // READ
    getAllExpenses():Promise<Expense[]>;
    getExpenseByPrimaryKey(primaryKey:number):Promise<Expense>;
    getAllExpensesByForeignKey(foreignKey:number):Promise<Expense[]>;
    // UPDATE
    updateExpense(expense:Expense):Promise<Expense>;
    // DELETE
    deleteExpenseByPrimaryKey(primaryKey:number):Promise<boolean>;
    deleteAllExpensesByForeignKey(foreignKey:number):Promise<boolean>;
}