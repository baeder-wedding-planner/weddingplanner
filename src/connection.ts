import { Client } from 'pg';

export const connect = new Client({
    user:'postgres',
    password:process.env.WEDDINGDBPASSWORD,
    database:'weddingdb',
    port:5432,
    host:'34.150.135.232'
})
connect.connect();