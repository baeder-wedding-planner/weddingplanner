import cors from 'cors';
import express from 'express';
import { Expense, Wedding } from './entities';
import { MissingResourceError } from './errors';
import ExpenseService from './services/expense-service';
import { ExpenseServiceImpl } from './services/expense-service-impl';
import WeddingService from './services/wedding-service';
import { WeddingServiceImpl } from './services/wedding-service-impl';

const app = express();
app.use(express.json());
app.use(cors());

const weddingService:WeddingService = new WeddingServiceImpl();
const expenseService:ExpenseService = new ExpenseServiceImpl();

app.get("/weddings", async (req, res)=>{
    try {
        const weddings:Wedding[] = await weddingService.retrieveAllWeddings();
        res.status(200);
        res.send(weddings);
    } catch (error) {
        res.status(404);
        res.send(error);
    }

})
app.get("/weddings/:id", async (req, res)=>{
    try {
        const id:number = Number(req.params.id);
        const wedding:Wedding = await weddingService.retrieveWeddingByPrimaryKey(id);
        res.send(wedding);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})
app.get("/weddings/:id/expenses", async (req, res)=>{
    try {
        const id:number = Number(req.params.id);
        const expenses:Expense[] = await expenseService.retrieveAllExpensesByForeignKey(id);
        res.send(expenses);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})
app.post("/weddings", async (req, res)=>{
    let wedding:Wedding = req.body;
    wedding = await weddingService.registerWedding(wedding);
    res.status(201);
    res.send(wedding);
})
app.delete("/weddings/:id", async (req, res) => {
    try {
        const id:number = Number(req.params.id);
        const ret = await weddingService.removeWeddingByPrimaryKey(id);
        res.send(`The wedding with ID ${id} as well as all associated expenses has been removed.`)
        res.status(205);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})
app.put("/weddings/:id", async (req, res) => {
    try {
        const id:number = Number(req.params.id);
        let wedding:Wedding = req.body;
        wedding.primaryKey=id
        wedding = await weddingService.modifyWedding(wedding);
        res.send(wedding);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})
app.get("/expenses", async (req, res)=>{
    try{
        const expenses:Expense[] = await expenseService.retrieveAllExpenses();
        res.status(200);
        res.send(expenses);
    } catch (error) {
        res.status(404);
        res.send(error);
    }
})
app.get("/expenses/:id", async (req, res)=>{
    try {
        const id:number = Number(req.params.id);
        const expense:Expense = await expenseService.retrieveExpenseByPrimaryKey(id);
        res.send(expense);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})
app.post("/expenses", async (req, res) => {
        let expense:Expense = req.body;
        expense = await expenseService.registerExpense(expense, expense.foreignKey);
        res.status(201);
        res.send(expense);
})
app.put("/expenses/:id", async (req, res) => {
    try {
        const id:number = Number(req.params.id);
        let expense:Expense = req.body;
        expense.primaryKey = id
        expense = await expenseService.modifyExpense(expense);
        res.send(expense);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})
app.delete("/expenses/:id", async (req, res) => {
    try {
        const id:number = Number(req.params.id);
        const ret = await expenseService.removeExpenseByPrimaryKey(id);
        res.send(`The expense with ID ${id} has been removed.`)
        res.status(205);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})
app.listen(3000,()=>{console.log("Application Started")});