export class Wedding{
    constructor(
        public primaryKey:number,
        public weddingDate:string,
        public locale:string,
        public lname:string,
        public budget:number
    ){}}
export class Expense{
    constructor(
        public primaryKey:number,
        public reason:string,
        public amount:number=0,
        public attachment:string,
        public foreignKey:number
    ){}
}